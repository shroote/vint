# README #

### Requirements ###
golang

### How to set up ###
go get gitlab.com/shroote/vint

./bin/build from application source folder

### Usage ###
Input file should be in same folder with executable. Then:

./vint [optional file param]

### Nice to have? ###
Bash script to run all tests:

./bin/test
