package discount

import (
	"gitlab.com/shroote/vint/src/dto"
	"gitlab.com/shroote/vint/src/shipment"
)

type third_l_shipment_via_lp_strategy struct {
	monthlyShipments map[string]int
	providers        []dto.Provider
}

func NewThirdLShipmentViaLpStrategy(providers []dto.Provider) *third_l_shipment_via_lp_strategy {
	s := new(third_l_shipment_via_lp_strategy)
	s.monthlyShipments = map[string]int{}
	s.providers = providers
	return s
}

func (s *third_l_shipment_via_lp_strategy) Calculate(month string, size shipment.Size, providerTitle string,
	price int,
) (bool, int) {
	if size != shipment.SIZE_L || providerTitle != "LP" {
		return false, 0
	}

	_, ok := s.monthlyShipments[month]
	if !ok {
		s.monthlyShipments[month] = 0
	}

	s.monthlyShipments[month]++

	if s.monthlyShipments[month] == 3 {
		return true, price
	}

	return false, 0
}
