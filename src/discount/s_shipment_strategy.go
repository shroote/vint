package discount

import (
	"gitlab.com/shroote/vint/src/dto"
	"gitlab.com/shroote/vint/src/shipment"
)

type s_shipment_strategy struct {
	providers []dto.Provider
}

func NewSShipmentStrategy(providers []dto.Provider) *s_shipment_strategy {
	s := new(s_shipment_strategy)
	s.providers = providers
	return s
}

func (s *s_shipment_strategy) Calculate(month string, size shipment.Size, providerTitle string, price int) (bool, int) {
	if size != shipment.SIZE_S {
		return false, 0
	}

	cheapestPrice := s.findCheapestPrice(s.providers)
	if cheapestPrice != -1 && price > cheapestPrice {
		return true, price - cheapestPrice
	}

	return false, 0
}

func (s *s_shipment_strategy) findCheapestPrice(providers []dto.Provider) int {
	result := -1
	for _, provider := range providers {
		price, ok := provider.ShippingPrices[shipment.SIZE_S]
		if !ok {
			continue
		}
		if result == -1 || price < result {
			result = price
		}
	}
	return result
}
