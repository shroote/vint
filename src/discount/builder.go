package discount

import (
	"gitlab.com/shroote/vint/src/dto"
)

func BuildDefaultStrategies(providers []dto.Provider) []Strategy {
	return []Strategy{
		NewSShipmentStrategy(providers),
		NewThirdLShipmentViaLpStrategy(providers),
	}
}
