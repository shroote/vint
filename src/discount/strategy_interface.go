package discount

import (
	"gitlab.com/shroote/vint/src/shipment"
)

type Strategy interface {
	Calculate(month string, size shipment.Size, providerTitle string, price int) (bool, int)
}
