package discount

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/shroote/vint/src/provider"
	"gitlab.com/shroote/vint/src/shipment"
	"testing"
)

func TestMakesThirdLSizeShipmentViaLpFreeOnceAMonth(t *testing.T) {
	discountStrategy := NewThirdLShipmentViaLpStrategy(provider.BuildDefaultProviders())

	isApplicable, discount := discountStrategy.Calculate("2019-03", shipment.SIZE_L, "LP", 690)
	assert.False(t, isApplicable)
	isApplicable, discount = discountStrategy.Calculate("2019-03", shipment.SIZE_L, "LP", 690)
	assert.False(t, isApplicable)
	isApplicable, discount = discountStrategy.Calculate("2019-03", shipment.SIZE_L, "LP", 690)
	assert.True(t, isApplicable)
	assert.Equal(t, 690, discount)

	isApplicable, discount = discountStrategy.Calculate("2019-03", shipment.SIZE_L, "LP", 690)
	assert.False(t, isApplicable)
	isApplicable, discount = discountStrategy.Calculate("2019-03", shipment.SIZE_L, "LP", 690)
	assert.False(t, isApplicable)
	isApplicable, discount = discountStrategy.Calculate("2019-03", shipment.SIZE_L, "LP", 690)
	assert.False(t, isApplicable)
}

func TestIsApplicableForLSizeOnly(t *testing.T) {
	discountStrategy := NewThirdLShipmentViaLpStrategy(provider.BuildDefaultProviders())

	isApplicable, _ := discountStrategy.Calculate("2019-03", shipment.SIZE_M, "LP", 690)
	assert.False(t, isApplicable)
	isApplicable, _ = discountStrategy.Calculate("2019-03", shipment.SIZE_M, "LP", 690)
	assert.False(t, isApplicable)
	isApplicable, _ = discountStrategy.Calculate("2019-03", shipment.SIZE_M, "LP", 690)
	assert.False(t, isApplicable)
}

func TestIsApplicableForLpProviderOnly(t *testing.T) {
	discountStrategy := NewThirdLShipmentViaLpStrategy(provider.BuildDefaultProviders())

	isApplicable, _ := discountStrategy.Calculate("2019-03", shipment.SIZE_L, "MR", 690)
	assert.False(t, isApplicable)
	isApplicable, _ = discountStrategy.Calculate("2019-03", shipment.SIZE_L, "MR", 690)
	assert.False(t, isApplicable)
	isApplicable, _ = discountStrategy.Calculate("2019-03", shipment.SIZE_L, "MR", 690)
	assert.False(t, isApplicable)
}

func TestIsApplicableForEachNewMonth(t *testing.T) {
	discountStrategy := NewThirdLShipmentViaLpStrategy(provider.BuildDefaultProviders())

	isApplicable, discount := discountStrategy.Calculate("2019-02", shipment.SIZE_L, "LP", 690)
	assert.False(t, isApplicable)
	isApplicable, discount = discountStrategy.Calculate("2019-02", shipment.SIZE_L, "LP", 690)
	assert.False(t, isApplicable)
	isApplicable, discount = discountStrategy.Calculate("2019-02", shipment.SIZE_L, "LP", 690)
	assert.True(t, isApplicable)
	assert.Equal(t, 690, discount)

	isApplicable, discount = discountStrategy.Calculate("2019-03", shipment.SIZE_L, "LP", 690)
	assert.False(t, isApplicable)
	isApplicable, discount = discountStrategy.Calculate("2019-03", shipment.SIZE_L, "LP", 690)
	assert.False(t, isApplicable)
	isApplicable, discount = discountStrategy.Calculate("2019-03", shipment.SIZE_L, "LP", 690)
	assert.True(t, isApplicable)
	assert.Equal(t, 690, discount)
}
