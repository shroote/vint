package discount

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/shroote/vint/src/provider"
	"gitlab.com/shroote/vint/src/shipment"
	"testing"
)

func TestFindsCheapestPriceForAnyProvider(t *testing.T) {
	discountStrategy := NewSShipmentStrategy(provider.BuildDefaultProviders())
	isApplicable, discount := discountStrategy.Calculate("2019-03", shipment.SIZE_S, "LP", 200)
	assert.True(t, isApplicable)
	assert.Equal(t, 50, discount)
	isApplicable, discount = discountStrategy.Calculate("2019-03", shipment.SIZE_S, "MR", 250)
	assert.True(t, isApplicable)
	assert.Equal(t, 100, discount)
}

func TestIsApplicableOnlyForSSize(t *testing.T) {
	discountStrategy := NewSShipmentStrategy(provider.BuildDefaultProviders())
	isApplicable, _ := discountStrategy.Calculate("2019-03", shipment.SIZE_S, "MR", 200)
	assert.True(t, isApplicable)
	isApplicable, _ = discountStrategy.Calculate("2019-03", shipment.SIZE_M, "MR", 200)
	assert.False(t, isApplicable)
	isApplicable, _ = discountStrategy.Calculate("2019-03", shipment.SIZE_L, "MR", 200)
	assert.False(t, isApplicable)
}

func TestIsNotApplicableIfPriceIsLowerThanDefaultProviderPrices(t *testing.T) {
	discountStrategy := NewSShipmentStrategy(provider.BuildDefaultProviders())
	isApplicable, _ := discountStrategy.Calculate("2019-03", shipment.SIZE_S, "LP", 100)
	assert.False(t, isApplicable)
	isApplicable, _ = discountStrategy.Calculate("2019-03", shipment.SIZE_S, "MR", 100)
	assert.False(t, isApplicable)
}
