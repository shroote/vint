package shipment

type Size string

const (
	SIZE_S Size = "S"
	SIZE_M Size = "M"
	SIZE_L Size = "L"
)
