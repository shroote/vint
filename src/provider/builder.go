package provider

import (
	"gitlab.com/shroote/vint/src/dto"
	"gitlab.com/shroote/vint/src/shipment"
)

func BuildDefaultProviders() []dto.Provider {
	return []dto.Provider{
		{Title: "LP", ShippingPrices: map[shipment.Size]int{
			shipment.SIZE_S: 150,
			shipment.SIZE_M: 490,
			shipment.SIZE_L: 690,
		}},
		{Title: "MR", ShippingPrices: map[shipment.Size]int{
			shipment.SIZE_S: 200,
			shipment.SIZE_M: 300,
			shipment.SIZE_L: 400,
		}},
	}
}
