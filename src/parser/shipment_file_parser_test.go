package parser

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/shroote/vint/src/shipment"
	"testing"
)

const INPUT = `2015-02-29 CUSPS
2015-03-01 S MR
`

func TestAllLinesAreParsed(t *testing.T) {
	shipments := ParseShipmentFile(INPUT)

	assert.Equal(t, 3, len(shipments))
}

func TestAllFieldsAreParsedForCorrectLine(t *testing.T) {
	shipments := ParseShipmentFile("2015-03-01 S MR")

	assert.Equal(t, "2015-03-01 S MR", shipments[0].Input)
	assert.Equal(t, "", shipments[0].Error)
	assert.Equal(t, "2015-03-01", shipments[0].Date)
	assert.Equal(t, shipment.SIZE_S, shipments[0].Size)
	assert.Equal(t, "MR", shipments[0].Provider)
}

func TestLinesWithIncorrectFieldCountGetError(t *testing.T) {
	shipments := ParseShipmentFile("2015-02-29 CUSPS")

	assert.Equal(t, "2015-02-29 CUSPS", shipments[0].Input)
	assert.Equal(t, "incorrect number of fields", shipments[0].Error)
}

func TestEmptyLinesGetError(t *testing.T) {
	shipments := ParseShipmentFile("")

	assert.Equal(t, "", shipments[0].Input)
	assert.Equal(t, "incorrect number of fields", shipments[0].Error)
}

func TestLineWithInvalidDateGetsError(t *testing.T) {
	shipments := ParseShipmentFile("2015/03/01 S MR")

	assert.Equal(t, "2015/03/01 S MR", shipments[0].Input)
	assert.Equal(t, "incorrect date format", shipments[0].Error)
}

func TestLineWithInvalidSizeGetsError(t *testing.T) {
	shipments := ParseShipmentFile("2015-03-01 XL MR")

	assert.Equal(t, "2015-03-01 XL MR", shipments[0].Input)
	assert.Equal(t, "incorrect shipment size", shipments[0].Error)
}
