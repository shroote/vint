package parser

import (
	"errors"
	"gitlab.com/shroote/vint/src/dto"
	"gitlab.com/shroote/vint/src/shipment"
	"regexp"
	"strings"
)

func ParseShipmentFile(contents string) []dto.Shipment {
	results := []dto.Shipment{}
	lines := strings.Split(contents, "\n")
	for _, line := range lines {
		results = append(results, parseLine(line))
	}
	return results
}

func isDateValid(date string) bool {
	r, err := regexp.Compile(`^[0-9]{4}-[0-9]{2}-[0-9]{2}$`)
	if err != nil {
		return false
	}

	if !r.MatchString(date) {
		return false
	}

	return true
}

func parseLine(line string) dto.Shipment {
	result := dto.Shipment{
		Input: line,
	}

	fields := strings.Split(line, " ")
	if len(fields) != 3 {
		result.Error = "incorrect number of fields"
		return result
	}

	if !isDateValid(fields[0]) {
		result.Error = "incorrect date format"
		return result
	}
	result.Date = fields[0]

	size, err := parseSize(fields[1])
	if err != nil {
		result.Error = err.Error()
		return result
	}
	result.Size = size

	result.Provider = fields[2]

	return result
}

func parseSize(size string) (shipment.Size, error) {
	switch size {
	case "S":
		return shipment.SIZE_S, nil
	case "M":
		return shipment.SIZE_M, nil
	case "L":
		return shipment.SIZE_L, nil
	default:
		return shipment.SIZE_S, errors.New("incorrect shipment size")
	}
}
