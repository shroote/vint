package ui

import (
	"fmt"
	"gitlab.com/shroote/vint/src/dto"
	"os"
)

func FatalError(err error) {
	Print("Error: " + err.Error())
	os.Exit(1)
}

func ParseArgs() string {
	if len(os.Args) == 1 {
		return ""
	}
	return os.Args[1]
}

func Print(message string) {
	fmt.Println(message)
}

func PrintShipments(shipments []dto.Shipment) {
	for _, shipment := range shipments {
		if shipment.Error != "" {
			Print(fmt.Sprintf("%s Ignored", shipment.Input))
		} else {
			Print(
				fmt.Sprintf("%s %s %s", shipment.Input, formatPrice(shipment.Price), formatDiscount(shipment.Discount)),
			)
		}
	}
}

func formatDiscount(amount int) string {
	if amount == 0 {
		return "-"
	}
	return formatPrice(amount)
}

func formatPrice(amount int) string {
	return fmt.Sprintf("%d.%02d", amount/100, amount%100)
}
