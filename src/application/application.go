package application

import (
	"gitlab.com/shroote/vint/src/calculator"
	"gitlab.com/shroote/vint/src/discount"
	"gitlab.com/shroote/vint/src/dto"
	"gitlab.com/shroote/vint/src/helper/date"
)

const AVAILABLE_MONTHLY_DISCOUNT = 1000

type application struct {
	availableMonthlyDiscounts map[string]int
	discountStrategies        []discount.Strategy
	providers                 []dto.Provider
}

func New(providers []dto.Provider, discountStrategies []discount.Strategy) *application {
	a := new(application)
	a.availableMonthlyDiscounts = map[string]int{}
	a.discountStrategies = discountStrategies
	a.providers = providers
	return a
}

func (a *application) ProcessShipments(shipments []dto.Shipment) []dto.Shipment {
	for i, shipment := range shipments {
		if shipment.Error != "" {
			continue
		}

		month := date.ParseMonthId(shipment.Date)
		availableMonthlyDiscount := a.getAvailableMonthlyDiscount(month)
		shipments[i] = a.processShipment(shipment, month, availableMonthlyDiscount)
		a.updateAvailableMonthlyDiscount(shipments[i], month)
	}
	return shipments
}

func (a *application) getAvailableMonthlyDiscount(month string) int {
	result, ok := a.availableMonthlyDiscounts[month]
	if !ok {
		result = AVAILABLE_MONTHLY_DISCOUNT
		a.availableMonthlyDiscounts[month] = result
	}
	return result
}

func (a *application) processShipment(shipment dto.Shipment, month string, availableMonthlyDiscount int) dto.Shipment {
	price, err := calculator.CalculatePrice(shipment, a.providers)
	if err != nil {
		shipment.Error = err.Error()
		return shipment
	}

	shipment.Price = price

	discount := calculator.CalculateDiscount(shipment, month, availableMonthlyDiscount, a.discountStrategies)
	if discount != -1 {
		shipment.Discount = discount
		shipment.Price = shipment.Price - discount
	}

	return shipment
}

func (a *application) updateAvailableMonthlyDiscount(shipment dto.Shipment, month string) {
	if shipment.Error == "" && shipment.Discount != 0 {
		a.availableMonthlyDiscounts[month] -= shipment.Discount
	}
}
