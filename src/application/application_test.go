package application

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/shroote/vint/src/discount"
	"gitlab.com/shroote/vint/src/dto"
	"gitlab.com/shroote/vint/src/parser"
	"gitlab.com/shroote/vint/src/provider"
	"testing"
)

const DEFAULT_INPUT = `2015-02-01 S MR
2015-02-02 S MR
2015-02-03 L LP
2015-02-05 S LP
2015-02-06 S MR
2015-02-06 L LP
2015-02-07 L MR
2015-02-08 M MR
2015-02-09 L LP
2015-02-10 L LP
2015-02-10 S MR
2015-02-10 S MR
2015-02-11 L LP
2015-02-12 M MR
2015-02-13 M LP
2015-02-15 S MR
2015-02-17 L LP
2015-02-17 S MR
2015-02-24 L LP
2015-02-29 CUSPS
2015-03-01 S MR`

func TestShipmentProcessing(t *testing.T) {
	providers := provider.BuildDefaultProviders()
	app := New(
		providers,
		discount.BuildDefaultStrategies(providers),
	)
	shipments := app.ProcessShipments(parser.ParseShipmentFile(DEFAULT_INPUT))

	assert.Equal(t, 21, len(shipments))
	checkShipment(t, 150, 50, "", shipments[0])
	checkShipment(t, 150, 50, "", shipments[1])
	checkShipment(t, 690, 0, "", shipments[2])
	checkShipment(t, 150, 0, "", shipments[3])
	checkShipment(t, 150, 50, "", shipments[4])
	checkShipment(t, 690, 0, "", shipments[5])
	checkShipment(t, 400, 0, "", shipments[6])
	checkShipment(t, 300, 0, "", shipments[7])
	checkShipment(t, 0, 690, "", shipments[8])
	checkShipment(t, 690, 0, "", shipments[9])
	checkShipment(t, 150, 50, "", shipments[10])
	checkShipment(t, 150, 50, "", shipments[11])
	checkShipment(t, 690, 0, "", shipments[12])
	checkShipment(t, 300, 0, "", shipments[13])
	checkShipment(t, 490, 0, "", shipments[14])
	checkShipment(t, 150, 50, "", shipments[15])
	checkShipment(t, 690, 0, "", shipments[16])
	checkShipment(t, 190, 10, "", shipments[17])
	checkShipment(t, 690, 0, "", shipments[18])
	checkShipment(t, 0, 0, "incorrect number of fields", shipments[19])
	checkShipment(t, 150, 50, "", shipments[20])
}

func checkShipment(t *testing.T, price, discount int, errorMessage string, shipment dto.Shipment) {
	assert.Equal(t, price, shipment.Price)
	assert.Equal(t, discount, shipment.Discount)
	assert.Equal(t, errorMessage, shipment.Error)
}
