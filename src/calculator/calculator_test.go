package calculator

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/shroote/vint/src/discount"
	"gitlab.com/shroote/vint/src/dto"
	"gitlab.com/shroote/vint/src/provider"
	"gitlab.com/shroote/vint/src/shipment"
	"testing"
)

func TestCalculatesPriceByProviderAndSize(t *testing.T) {
	dto := dto.Shipment{
		Provider: "LP",
		Size:     shipment.SIZE_L,
	}
	price, err := CalculatePrice(dto, provider.BuildDefaultProviders())
	assert.Nil(t, err)
	assert.Equal(t, 690, price)
}

func TestReturnsPriceErrorForUnknownProvider(t *testing.T) {
	dto := dto.Shipment{
		Provider: "PL",
		Size:     shipment.SIZE_L,
	}
	_, err := CalculatePrice(dto, provider.BuildDefaultProviders())
	assert.NotNil(t, err)
	assert.Equal(t, "provider not found", err.Error())
}

func TestReturnsPriceErrorForUnknownSize(t *testing.T) {
	dto := dto.Shipment{
		Provider: "LP",
		Size:     "XL",
	}
	_, err := CalculatePrice(dto, provider.BuildDefaultProviders())
	assert.NotNil(t, err)
	assert.Equal(t, "provider does not support this shipment size", err.Error())
}

func TestCalculatesDiscountFromAvailableDiscountStrategies(t *testing.T) {
	dto := dto.Shipment{
		Price:    200,
		Provider: "MR",
		Size:     shipment.SIZE_S,
	}
	discount := CalculateDiscount(
		dto, "2019-03", 1000, discount.BuildDefaultStrategies(provider.BuildDefaultProviders()),
	)
	assert.Equal(t, 50, discount)
}

func TestCalculatesLowerDiscountIfAvailableMonthlyDiscountIsLower(t *testing.T) {
	dto := dto.Shipment{
		Price:    200,
		Provider: "MR",
		Size:     shipment.SIZE_S,
	}
	discount := CalculateDiscount(
		dto, "2019-03", 25, discount.BuildDefaultStrategies(provider.BuildDefaultProviders()),
	)
	assert.Equal(t, 25, discount)
}
