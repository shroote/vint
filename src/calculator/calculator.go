package calculator

import (
	"errors"
	"gitlab.com/shroote/vint/src/discount"
	"gitlab.com/shroote/vint/src/dto"
)

func CalculateDiscount(shipment dto.Shipment, month string, availableMonthlyDiscount int,
	discountStrategies []discount.Strategy,
) int {
	result := -1
	for _, discountStrategy := range discountStrategies {
		isApplicable, discount := discountStrategy.Calculate(month, shipment.Size, shipment.Provider, shipment.Price)
		if isApplicable && (result == -1 || discount < result) {
			result = discount
			if result > availableMonthlyDiscount {
				result = availableMonthlyDiscount
			}
		}
	}
	return result
}

func CalculatePrice(shipment dto.Shipment, providers []dto.Provider) (int, error) {
	var result int
	found := false
	for _, provider := range providers {
		if shipment.Provider == provider.Title {
			found = true
			price, ok := provider.ShippingPrices[shipment.Size]
			if !ok {
				return result, errors.New("provider does not support this shipment size")
			}
			result = price
			break
		}
	}

	if !found {
		return result, errors.New("provider not found")
	}

	return result, nil
}
