package dto

import (
	"gitlab.com/shroote/vint/src/shipment"
)

type Shipment struct {
	Date     string
	Discount int
	Error    string
	Input    string
	Price    int
	Provider string
	Size     shipment.Size
}
