package dto

import (
	"gitlab.com/shroote/vint/src/shipment"
)

type Provider struct {
	Title          string
	ShippingPrices map[shipment.Size]int
}
