package date

func ParseMonthId(date string) string {
	if len(date) < 7 {
		return date
	}
	return date[:7]
}
