package date

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestParsesIdFromCorrectlyFormattedDate(t *testing.T) {
	assert.Equal(t, "2019-03", ParseMonthId("2019-03-03"))
}

func TestReturnsShorterIdIfDateIsTooShort(t *testing.T) {
	assert.Equal(t, "", ParseMonthId(""))
}
