package filesystem

import (
	"errors"
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
)

func GetCurrentPath() (string, error) {
	path, err := filepath.Abs(filepath.Dir(os.Args[0]))
	if err != nil {
		return "", err
	}
	return path + string(os.PathSeparator), nil
}

func PathExists(filePath string) bool {
	_, err := os.Stat(filePath)
	if err == nil {
		return true
	}
	return false
}

func ReadFile(filePath string) (string, error) {
	if !PathExists(filePath) {
		return "", errors.New(fmt.Sprintf("file \"%s\" doesn't exist", filePath))
	}
	data, err := ioutil.ReadFile(filePath)
	if err != nil {
		return "", err
	}
	return string(data), nil
}
