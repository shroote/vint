package filesystem

import (
	"github.com/stretchr/testify/assert"
	"reflect"
	"testing"
)

const DIR_TEST_DATA = "_test_data"
const FILE_READ = DIR_TEST_DATA + "/filesystem_test_read.txt"
const FILE_NONEXISTENT = DIR_TEST_DATA + "/filesystem_test_nonexistent.txt"

func TestGetCurrentPath(t *testing.T) {
	path, err := GetCurrentPath()
	if err != nil {
		assert.FailNow(t, err.Error())
	}
	assert.Equal(t, "string", reflect.TypeOf(path).Name())
}

func TestPathExists(t *testing.T) {
	assert.True(t, PathExists(FILE_READ))
	assert.False(t, PathExists(FILE_NONEXISTENT))
	assert.True(t, PathExists(DIR_TEST_DATA))
	assert.True(t, PathExists(DIR_TEST_DATA+"/"))
	assert.False(t, PathExists("_nonexistent_dir"))
}

func TestReadExistingFile(t *testing.T) {
	data, err := ReadFile(FILE_READ)
	if err != nil {
		assert.FailNow(t, err.Error())
	}
	assert.Equal(t, "read test", data)
}

func TestReadNonexistentFile(t *testing.T) {
	_, err := ReadFile(FILE_NONEXISTENT)
	if err == nil {
		assert.FailNow(t, "Expected error for nonexistent file")
	}
	assert.Equal(t, "file \"_test_data/filesystem_test_nonexistent.txt\" doesn't exist", err.Error())
}
