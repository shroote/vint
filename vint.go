package main

import (
	"fmt"
	"gitlab.com/shroote/vint/src/application"
	"gitlab.com/shroote/vint/src/discount"
	"gitlab.com/shroote/vint/src/helper/filesystem"
	"gitlab.com/shroote/vint/src/parser"
	"gitlab.com/shroote/vint/src/provider"
	"gitlab.com/shroote/vint/src/ui"
)

const DEFAULT_INPUT = "input.txt"

func main() {
	path, err := filesystem.GetCurrentPath()
	if err != nil {
		ui.FatalError(err)
	}

	file := ui.ParseArgs()
	if file == "" {
		file = DEFAULT_INPUT
	}
	ui.Print(fmt.Sprintf("Parsing file %s:", file))

	contents, err := filesystem.ReadFile(path + file)
	if err != nil {
		ui.FatalError(err)
	}

	providers := provider.BuildDefaultProviders()
	app := application.New(
		providers,
		discount.BuildDefaultStrategies(providers),
	)
	shipments := app.ProcessShipments(parser.ParseShipmentFile(contents))
	ui.PrintShipments(shipments)
}
